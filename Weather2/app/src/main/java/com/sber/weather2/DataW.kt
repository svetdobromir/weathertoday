package com.sber.weather2

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
data class WeatherData(
    val coord: Coord = Coord(0.0, 0.0),
    val weather: List<Weather> = emptyList(),
    val base: String = "-",
    val main: Main = Main(0.0, 0.0, 0.0, 0.0, 0, 0, 0, 0),
    val visibility: Int = 0,
    val wind: Wind = Wind(0.0, 0, 0.0),
    val clouds: Clouds = Clouds(0),
    val rain: Rain = Rain(0.0, 0.0),
    val snow: Snow = Snow(0.0, 0.0),
    val dt: Long = 0,
    val sys: Sys = Sys(0, 0, "-", 0, 0),
    val timezone: Int = 0,
    val id: Int = 0,
    val name: String = "-",
    val cod: Int = 0,
    @Transient
    var status: String = "start"

)


@Serializable
data class Rain(
    @SerialName("1h")
    val hour1: Double = 0.0,
    @SerialName("3h")
    val hour3: Double = 0.0
)

@Serializable
data class Snow(
    @SerialName("1h")
    val hour1: Double = 0.0,
    @SerialName("3h")
    val hour3: Double = 0.0
)

@Serializable
data class Coord(
    val lon: Double,
    val lat: Double
)

@Serializable
data class Weather(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
)

@Serializable
data class Main(
    val temp: Double,
    @SerialName("feels_like")
    val feelsLike: Double,
    @SerialName("temp_min")
    val tempMin: Double,
    @SerialName("temp_max")
    val tempMax: Double,
    val pressure: Int,
    val humidity: Int,
    @SerialName("sea_level")
    val seaLevel: Int,
    @SerialName("grnd_level")
    val grndLevel: Int
)

@Serializable
data class Wind(
    val speed: Double,
    val deg: Int,
    val gust: Double
)

@Serializable
data class Clouds(
    val all: Int
)

@Serializable
data class Sys(
    val type: Int,
    val id: Int,
    val country: String,
    val sunrise: Long,
    val sunset: Long
)
