package com.sber.weather2


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.internal.wait
import java.io.IOException


fun getWeather(town: String = "Moscow"): WeatherData {
    var data = WeatherData()
    val apiKey = "3d7f816d9a65d237cd2d079239431615"
    val url = "https://api.openweathermap.org/data" +
            "/2.5/weather?q=$town&appid=$apiKey&units=metric&lang=ru"
    val client = OkHttpClient();
    val request = Request.Builder()
        .url(url)
        .build()

    var responseBody: String? = ""

    try {
        val def = CoroutineScope(Dispatchers.IO).launch {
            client.newCall(request).execute().use { response ->
                if (!response.isSuccessful)
                    throw IOException("Unexpected code $response")
                responseBody = response.body?.string()
            }
        }
        while (def.isActive)

            if (responseBody.isNullOrEmpty()) {
                data.status = "error";
            } else {
                data = Json.decodeFromString(
                    WeatherData.serializer(),
                    responseBody!!
                )
                data.status = "ok"
            }
    } catch (ex: IOException) {
        data.status = "error";
    }
    return data;
}
