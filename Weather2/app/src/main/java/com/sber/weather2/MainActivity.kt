package com.sber.weather2

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HomeScreen()
        }
    }
}


@Composable
private fun HomeScreen() {

    val isLoading = remember { mutableStateOf(false) }
    val weatherObj = remember { mutableStateOf(WeatherData()) }

    LaunchedEffect(isLoading.value) {
        if (isLoading.value) {
            weatherObj.value = getWeather();
            isLoading.value = false
        }
    }

    var buttonText: String = ""
    when (weatherObj.value.status) {
        "ok" -> buttonText = stringResource(R.string.button_request_ok)
        "error" -> buttonText = stringResource(R.string.button_request_error)
        "start" -> buttonText = stringResource(R.string.button_request)
    }

    var weatherDescription: String = "---"
    if (weatherObj.value.weather.isNotEmpty()) {
        weatherDescription = weatherObj
            .value.weather[0].description
    }
    if (!isLoading.value) {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {

            Card(
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 10.dp
                ),
                modifier = Modifier.padding(10.dp),
                colors = CardDefaults.cardColors(
                    containerColor = Color.LightGray,
                    contentColor = Color.White
                )
            ) {
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Text(text = "Moscow", fontSize = 28.sp, color = Color.Black)
                    Text(text = weatherDescription, fontSize = 14.sp, color = Color.Black)
                }
            }

            Card(
                modifier = Modifier
                    .padding(
                        horizontal = 10.dp,
                        vertical = 10.dp
                    ),
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 10.dp,
                ), colors = CardDefaults.cardColors(
                    containerColor = Color.LightGray,
                    contentColor = Color.White
                )
            ) {
                Column(
                    verticalArrangement = Arrangement.SpaceBetween
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        TypeOneBox(
                            stringResource(R.string.humadity_rus),
                            weatherObj.value.main.humidity.toString() + "%"
                        )
                        TypeTwoBox(
                            "${stringResource(R.string.feeling_rus)}  ${weatherObj.value.main.feelsLike}",
                            "${stringResource(R.string.max_temp)} ${weatherObj.value.main.tempMin}",
                            "${stringResource(R.string.min_temp)} ${weatherObj.value.main.tempMax}"
                        )
                        TypeOneBox(
                            stringResource(R.string.temp),
                            weatherObj.value.main.temp.toInt().toString() + "℃"
                        )
                    }
                }
            }

            Card(
                modifier = Modifier
                    .padding(
                        horizontal = 10.dp,
                        vertical = 10.dp,

                        ),
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 10.dp,
                ),
                colors = CardDefaults.cardColors(
                    containerColor = Color.LightGray,
                    contentColor = Color.White
                )

            ) {
                Column(
                    verticalArrangement = Arrangement.SpaceBetween
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        TypeOneBox(
                            stringResource(R.string.cloudy),
                            "${weatherObj.value.clouds.all} %"
                        )
                        TypeTwoBox(
                            "${stringResource(R.string.wing_strong)}    ${weatherObj.value.main.feelsLike}",
                            "${stringResource(R.string.degr)}        ${weatherObj.value.main.tempMin}",
                            "${stringResource(R.string.direction_wind)}   ${weatherObj.value.main.tempMax}"
                        )
                        TypeOneBox(
                            stringResource(R.string.pressure),
                            weatherObj.value.main.pressure.toString() + " Гпа"
                        )
                    }
                }
            }
            Card(
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 10.dp
                ),
                modifier = Modifier.padding(10.dp),
                colors = CardDefaults.cardColors(
                    containerColor = Color.LightGray,
                    contentColor = Color.White
                ),

                ) {
                Row(
                    verticalAlignment = Alignment.Bottom,
                    horizontalArrangement = Arrangement.Center,
                    modifier = Modifier.background(Color.LightGray)
                ) {

                    FilledTonalButton(buttonText = buttonText) {

                        isLoading.value = true;
                    }
                }
            }
        }
    } else {
        Row(verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
        CircularProgressIndicator(color = Color.Red,
            modifier = Modifier.width(64.dp),
            trackColor = Color.Red,
            )
        }
    }
}


@Composable
fun TypeOneBox(
    value1: String,
    value2: String
) {
    Box(modifier = Modifier) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(text = value1, color = Color.Black)
            Text(value2, fontSize = 20.sp, color = Color.Black)
        }
    }
}

@Composable
fun TypeTwoBox(feelsLikeTemp: String, maxTemp: String, minTemp: String) {
    Box(contentAlignment = Alignment.Center) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceAround
        ) {
            Text(text = maxTemp, color = Color.Black)
            Text(text = minTemp, color = Color.Black)
            Text(text = feelsLikeTemp, color = Color.Black)
        }
    }
}


@Composable
fun FilledTonalButton(
    buttonText: String,
    onClick: () -> Unit
) {
    FilledTonalButton(onClick = { onClick() }) {
        Text(buttonText)
    }
}
